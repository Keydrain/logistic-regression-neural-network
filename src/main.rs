extern crate image;
extern crate resize;
extern crate rand;

use std::f64;
use std::f64::consts::E;
use std::fs::read_dir;
// use std::fs::File;
// use std::path::PathBuf;
use std::cmp::min;
use image::GenericImage;
use image::Lanczos3;
use rand::Rng;

fn main() {
    println!{""};
    let cat_data: Vec<(String, Vec<f64>)> = load_data("testData", 16, "Cat")
        .iter()
        .map(|x| (x.0.clone(), x.1.clone()))
        .collect();

    let theta: Vec<f64> = intialize_weight((&cat_data[0]).1.len());

    // let offset = 0.0; // Suggested
    let offset = -16.0 * 16.0 * 3.0;
    let guesses: Vec<(&String, f64)> = cat_data
        .iter()
        .map(|ref x| (&x.0, calc_hypothesis(&x.1, &theta, offset)))
        .collect();

    println!("Initial Guess: {:?}", guesses);

    // Still need to train the weights and include a more diverse dataset
}

/// Returns a prediction (percent) based on the provided _data_ and _theta_s as well as an _offset_
fn calc_hypothesis(data: &Vec<f64>, theta: &Vec<f64>, offset: f64) -> f64 {
    Math::sigmoid(data.iter().zip(theta.iter()).map(|(&x, &y)| x * y).fold(
        offset,
        |sum,
         x| {
            sum + x
        },
    ))
}

/// Returns a vector of weights which should be initialized to 0.0
/// For testing purposes, the weights are randomly initialized between 0.0 and 1.0
fn intialize_weight(dim: usize) -> Vec<f64> {
    vec![0.0; dim]
        .iter()
        .map(|_| rand::thread_rng().gen_range(0.0, 1.0))
        .collect()
}

/// Performs 3 operations:
/// * Loads the data from the provided _file_path_
/// * Crops images to centered squares
/// * Resizes the square to the specified _dim_
/// Returns a vector of PathBuf and raw pixel array
fn load_data(file_path: &str, dim: u32, thing: &str) -> Vec<(String, Vec<f64>)> {
    read_dir(file_path)
        .unwrap()
        .map(|x| x.unwrap())
        .map(|x| (x.file_name().into_string().unwrap(), x.path()))
        .map(|(x, y)| (x, image::open(y.as_path()).unwrap()))
        .map(|(x, mut y)| {
            let min = min(y.dimensions().0, y.dimensions().1) as u32;
            let width = ((y.dimensions().0 - min) as f64 / 2.0) as u32;
            let height = ((y.dimensions().1 - min) as f64 / 2.0) as u32;
            (x, y.crop(width, height, min, min))
        })
        .map(|(x, y)| {
            (
                thing.to_owned() + "-" + &x,
                y.resize(dim, dim, Lanczos3)
                    .raw_pixels()
                    .iter()
                    .map(|&x| x as f64 / 255.0)
                    .collect(),
            )
        })
        .collect()
}

trait Math {
    fn sigmoid(self) -> f64;
}

impl Math for f64 {
    fn sigmoid(self) -> f64 {
        1.0 / (1.0 + E.powf(-1.0 * self))
    }
}

impl Math for i32 {
    fn sigmoid(self) -> f64 {
        1.0 / (1.0 + E.powf(-1.0 * self as f64))
    }
}